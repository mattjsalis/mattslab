package expressionparser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * <pre>
 * Nodal data structure that holds a "compiled" version of a user defined expression.
 * <br><br>Each node can be be resolved down to a single Double output. A node contains
 * either one, two or no child nodes. Node resolution occurs through the following steps
 * <br>1. Child nodes are resolved into a single Double. 
 * <br>    - If there is only one child node, then it is called to resolve itself.
 * <br>    - If there are two child nodes, then they are each called to resolve themselves
 *      and a BiFunction is called on the result of their resolutions.
 * <br>    - If there are no children, then the node must be a numeric endpoint containing a
 *      a Double value (numericValue). This value is returned.
 * <br>2. A series of Function(s) are applied to the value obtained by resolving the child nodes
 * and this value is provided as the output of the node.
 * 
 * <br><br><b>Note:</b>Functions are applied sequentially to each other's output.
 * <br>For example, functions [f(x),g(x),h(x)] would be applied as h(g(f(x))) where x is the
 * numeric resolution of the child node(s).
 * </pre>
 * @author Matt
 *
 */
public class ExpressionNode {
	
	public Map<String,Double> variableMap = null;
	public String expression = null;
	//Function to be applied to this node's reconciliation and the reconciliation of it's next sibling
	private BiFunction<Double,Double,Double> siblingFunction = null;
	//Child nodes of this node
	private List<ExpressionNode> childNodes = new ArrayList<ExpressionNode>();
	//Functions to be applied to the result of child node reconciliation
	private List<Function<Double,Double>> functionalExpressions = new ArrayList<Function<Double,Double>>();

	private Object value = null;
	public ExpressionNode addFunctionalExppression(Function<Double,Double> func){
		this.functionalExpressions.add(func);
		return this;
	}
	public void seedValue(Object value){
		this.value = value;
	}
	
	public ExpressionNode addChild(ExpressionNode childNode){
		this.childNodes.add(childNode);
		return this;
	}
	public void addSiblingFunction(BiFunction<Double,Double,Double> sibFunc){
		this.siblingFunction = sibFunc;
	}
	
	
	public Double solve(){
		return solve(variableMap);
	}
	public Double solve(Map<String,Double> variables){
		Double reconciledSelfValue = 0.0;
		if (childNodes.isEmpty() && value instanceof String){
			reconciledSelfValue = variables.get(value);
		} else if(childNodes.isEmpty()){
			return (Double) value;
		} else {
			reconciledSelfValue = this.solveChildNodes(variables);
		}
		
		for (Function<Double, Double> function : functionalExpressions){
			reconciledSelfValue = function.apply(reconciledSelfValue);
		}
		return reconciledSelfValue;
	}
	private Double solveChildNodes(Map<String,Double> variables){
		//Apply functions between siblings
		if (childNodes.size() == 1){
			return childNodes.get(0).solve(variables);
		} else {
			return siblingFunction.apply(childNodes.get(0).solve(variables), childNodes.get(1).solve(variables));
		}
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public void setVariableMap(Map<String,Double> varMap){
		this.variableMap = varMap;
	}
	public Map<String,Double> getVariableMap(){
		return this.variableMap;
	}
}
