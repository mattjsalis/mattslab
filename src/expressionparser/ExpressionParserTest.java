package expressionparser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

public class ExpressionParserTest {
	
	private double allowableErr = 1E-4;
	
	@Test
	public void testExpressionParserNumeric(){
		 List<Pair<String,Double>> testExpressions = Arrays.asList(
			 new Pair<String,Double>("1 + 1",2.0),
			 new Pair<String,Double>("1/(3.0*sqrt(3.14159))*exp(-0.5*((1.0-0.0)/3)^2)",0.1779),
			 new Pair<String,Double>("-(sin(1)^2+cos(1)^2)",-1.0),
			 new Pair<String,Double>("10 + (20 - 10)/(5)*(2.5)",15.0),
			 new Pair<String,Double>("1/2 + 3*4 + 2^-(7-2)",12.5313),
			 new Pair<String,Double>("exp(-0.5)^2/sin(3)^6",4.657726277049783e+04),
			 new Pair<String,Double>("-(1 +exp(-0.5)^2)/(sin(3)^6/tan(3))",2.468726371704029e+04),
			 new Pair<String,Double>("cos(-(1 +exp(-0.5)^2)/(sin(3)^6/tan(3)))",0.8088),
			 new Pair<String,Double>("cos(-(sin(2))/(sin(2)))",0.5403),
			 new Pair<String,Double>("3/2*((sin(1)*cos(1))^2)",0.3101),
			 new Pair<String,Double>("3^0.23/2^-2.3*((sin(1)*cos(1))^2)",1.3106),
			 new Pair<String,Double>("3^0.23/-2^-2.3*((sin(1)*cos(1))^2)",-1.3106)
		 );
		 //ExpressionNode parentNode = new ExpressionNode();
		 for (Pair<String,Double> expPair : testExpressions){
			 ExpressionNode parentNode = new ExpressionNode();
			 (new ExpressionParser()).compileExpression(expPair.getFirst(),parentNode);
			 double calculatedValue = 0.0;
			 try{
			 calculatedValue = parentNode.solve();
			 } catch(Exception e){
			 System.out.println("FAILED: Could not compile " +
			 expPair.getFirst());
			 continue;
			 }
			 double err = Math.abs(expPair.getSecond() - calculatedValue);
			 if (err > allowableErr){
			 System.out.println("FAILED: " + "\"+ expPair.getFirst() + \" returned value that is"
				 + " more than allowable error."
				 + "\n\tCalculated value: " + String.valueOf(calculatedValue)
				 + "\n\t Expected value: " + String.valueOf(expPair.getSecond()));
			 } else {
			 System.out.println("PASSED: " + "\"" + expPair.getFirst() + "\" +  meets error requirements");
			 }
		 }
	}
	
	@Test
	public void testExpressionNodesymbolic(){
		 /**
		 * Symbolic variable tests
		 */
		HashMap<String, Double> syms = new HashMap<String, Double>();
		double x = -10;
		double y = 2.0;
		syms.put("x", x);
		syms.put("y", y);

		List<Pair<String, Double>> symTestExpressions = Arrays.asList(
				new Pair<String,Double>("-x",10.0),
				new Pair<String, Double>("x/y + x*y + x^(x-y)", -25.0),
				new Pair<String, Double>("x + y", -8.0),
				new Pair<String, Double>("x^8/(y*sqrt(3.14159))*exp(-0.5*((1.0-x)/y)^2)", 7.6154),
				new Pair<String, Double>("-(sin(x)^2+cos(y)^2)", -0.4691),
				new Pair<String, Double>("x + (y - 10)/(5)*(y-1)", -11.6),

				new Pair<String, Double>("exp(-y)^2/sin(x)^6", 0.7065),
				new Pair<String, Double>("-(x + exp(-y)^2)/(sin(x)^6/tan(x))", -249.6468),
				new Pair<String, Double>("cos(-(1 + exp(-y)^2)/(sin(y)^6/tan(x)))", 0.3919),
				new Pair<String, Double>("cos(-(sin(x))/(sin(y)))", 0.8263),
				new Pair<String, Double>("x/y*((sin(x)*cos(y))^2)", -0.2563),
				new Pair<String, Double>("y^x/2^-2.3*((sin(y)*cos(x))^2)", 0.0028));
		// ExpressionNode parentNode = new ExpressionNode();
		for (Pair<String, Double> expPair : symTestExpressions) {
			ExpressionNode parentNode = new ExpressionNode();
			(new ExpressionParser()).compileExpression(expPair.getFirst(), parentNode);
			double calculatedValue = 0.0;
			try {
				calculatedValue = parentNode.solve(syms);
			} catch (Exception e) {
				System.out.println("FAILED: Could not compile " + expPair.getFirst());
				continue;
			}
			double err = Math.abs(expPair.getSecond() - calculatedValue);
			if (err > allowableErr) {
				System.out.println("FAILED: " + "\"" + expPair.getFirst() + "\" returned value that is"
						+ " more than allowable error." + "\n\tCalculated value: " + String.valueOf(calculatedValue)
						+ "\n\t  Expected value: " + String.valueOf(expPair.getSecond()));
			} else {
				System.out.println("PASSED: " + "\"" + expPair.getFirst() + "\"" + " meets error requirements");
			}
		}
	}
}
