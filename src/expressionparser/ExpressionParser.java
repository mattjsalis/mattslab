package expressionparser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class serves to compile an algebraic expression (in the form of
 * a raw String) into an Java object (ExpressionNode) with capability to 
 * return the numeric solution for the expression.
 * 
 * <br>
 * <br>
 * <b>Supported algebraic expressions:</b> <br>
 * ^ : Exponent <br>
 * * : Multiplication <br>
 * / : Division <br>
 * + : Addition <br>
 * - : Subtraction
 * 
 * <br>
 * <br>
 * The class works by first resolving expressions within parenthesis starting
 * with the parenthetical expression furthest to the right in the string in a
 * recursive manner. * <br>
 * <br>
 * The code utilizes <b>Reflection</b> to call unary methods from the <b>Math</b>
 * class. Strings within the expression such as "sin" or "cos" are interpreted
 * as method calls to <b>Math</b>.
 * <br>
 * For example, Math.cos(1.5) may be called in the String as "cos(1.5)"
 * <br>
 * <br>
 * Any method calls to a parenthetic expression will be called first followed by
 * any exponential calls. <br>
 * <br>
 * For example, for an expression "cos(2+4)^2": <br>
 * <br>
 * cos(2+4)^2 will first be resolved to cos(6)^2, then 0.9602^2 and then finally
 * 0.9220 <br>
 * <br>
 * <br>
 * <b>Note:</b>This class was modeled off of MATLAB-style algebraic expressions.
 * In fact, many expressions input to this class should be able to be directly
 * solved by MATLAB with the limiting factor being that MATLAB has an equivalent
 * operation to the Math method called.
 * 
 * @author Matt
 *
 */
public class ExpressionParser {

	public static final String numericRegEx = "\\d+(,\\d+)*(\\.\\d+(e\\d+)?)?";
	public static final String negNumericRegExp = "\\-?" + numericRegEx;
	private static final String EXP_NODE = "DERIVED_NODE_KEY";
	private static final String NODE = "NODE_KEY";

	private static LinkedHashMap<String, BiFunction<Double, Double, Double>> doubleOperationMap = createDoubleReturnOperationMap();

	private static LinkedHashMap<String, BiFunction<Double, Double, Double>> createDoubleReturnOperationMap() {
		BiFunction<Double, Double, Double> exponent = (x1, x2) -> {
			return Math.pow(Double.valueOf(x1), Double.valueOf(x2));
		};
		BiFunction<Double, Double, Double> multiply = (x1, x2) -> {
			return Double.valueOf(x1) * Double.valueOf(x2);
		};
		BiFunction<Double, Double, Double> divideBy = (x1, x2) -> {
			return Double.valueOf(x1) / Double.valueOf(x2);
		};
		BiFunction<Double, Double, Double> plus = (x1, x2) -> {
			return Double.valueOf(x1) + Double.valueOf(x2);
		};
		BiFunction<Double, Double, Double> minus = (x1, x2) -> {
			return Double.valueOf(x1) - Double.valueOf(x2);
		};
		LinkedHashMap<String, BiFunction<Double, Double, Double>> operationMap = new LinkedHashMap<String, BiFunction<Double, Double, Double>>();
		operationMap.put("^", exponent);
		operationMap.put("/", divideBy);
		operationMap.put("*", multiply);
		operationMap.put("+", plus);
		operationMap.put("-", minus);
		return operationMap;
	}

	private static HashMap<String, Function<Double, Double>> mathFunctionMap = createMathFunctionMap();

	private static HashMap<String, Function<Double, Double>> createMathFunctionMap() {
		HashMap<String, Function<Double, Double>> mathFunctionMap = new HashMap<String, Function<Double, Double>>();
		HashMap<String, Method> mathMap = getMathMethodsReturnDouble();
		for (Entry<String, Method> mathMethod : mathMap.entrySet()) {
			Function<Double, Double> mathFunc = (aDub) -> {
				try {
					return (Double) mathMethod.getValue().invoke(Math.class, aDub);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
					return null;
				}
			};
			mathFunctionMap.put(mathMethod.getKey(), mathFunc);
		}
		// Add function to negate an double
		Function<Double, Double> negate = aDub -> {
			return -aDub;
		};
		mathFunctionMap.put("NEGATE", negate);
		return mathFunctionMap;
	}

	private static HashMap<String, Method> getMathMethodsReturnDouble() {
		HashMap<String, Method> methodsReturnDouble = (HashMap<String, Method>) Stream
				.of(Math.class.getDeclaredMethods())
				.filter(method -> method.getReturnType().getTypeName().equals("double"))
				.collect(Collectors.toMap(Method::getName, Function.identity()));
		return methodsReturnDouble;
	}

	/**
	 * Method to return the first parenthetical statement in the expression. If a
	 * function is called on the parenthetical (ie sin, cos, etc.) then the function
	 * is returned as well.
	 * @param aString
	 * @return contents of the first parenthetical statement in the expression
	 */
	private String getFirstTopLevelParenthetical(String aString) {
		// Find the first parenthetical
		String firstParenthetical = null;
		int firstopenParInd = aString.indexOf("(");
		if (firstopenParInd >= 0) {
			int internalOpenParCount = 0;
			for (int i_char = firstopenParInd; i_char < aString.length(); i_char++) {
				if (aString.charAt(i_char) == ')') {
					internalOpenParCount--;
				}
				if (aString.charAt(i_char) == ')' && internalOpenParCount == 0) {
					// parenthetical statement has been found. look for method
					// call
					Matcher m = Pattern.compile("[A-Za-z]+[\\d+]?[[A-Za-z]+]?")
							.matcher(aString.substring(0, firstopenParInd));
					String lastWord = null;
					while (m.find()) {
						lastWord = m.group();
					}

					firstParenthetical = aString.substring(firstopenParInd, i_char + 1);
					if (lastWord != null) {
						String charsBeforeParenthetical = aString.substring(firstopenParInd - lastWord.length(),
								firstopenParInd);
						if (charsBeforeParenthetical.equals(lastWord)) {
							firstParenthetical = lastWord + firstParenthetical;
						}
					}
					break;
				}
				if (aString.charAt(i_char) == '(') {
					internalOpenParCount++;
				}
			}
		}
		return firstParenthetical;
	}
	
	/**
	 * Method to compile the raw string into an ExpressionNode
	 * @param aString
	 * @param expNode
	 * @return
	 */
	public ExpressionNode compileExpression(String aString, ExpressionNode expNode) {
		HashMap<String, ExpressionNode> nodeMap = new HashMap<String, ExpressionNode>();
		aString = this.prepareExpressionString(aString);
		// aString = this.prepareExpressionString(aString);
		String firstParenthetical = getFirstTopLevelParenthetical(aString);
		int i_par = 0;
		while (firstParenthetical != null) {
			// Create a new expression node child
			ExpressionNode childNode = new ExpressionNode();
			childNode.setExpression(firstParenthetical);
			// Create node key
			String nodeKey = ExpressionParser.NODE + String.valueOf(i_par);
			nodeMap.put(nodeKey, childNode);
			// If the first character of the first parenthetical is not an open
			// parenthesize, try to get the method call
			int indFirstopenPar = firstParenthetical.indexOf('(');
			if (firstParenthetical.charAt(0) != '(') {
				String methodName = firstParenthetical.substring(0, indFirstopenPar);
				Function<Double, Double> mathFunc = ExpressionParser.mathFunctionMap.get(methodName);
				childNode.addFunctionalExppression(mathFunc);
			}
			// If the contents of the parenthetical also hold parenthesizes then
			// call this method on it
			String contents = firstParenthetical.substring(indFirstopenPar + 1, firstParenthetical.length() - 1);
			if (contents.contains("(")) {
				compileExpression(contents, childNode);
			} else {
				// If the statement has no parenthetical then standard order of
				// operations may be applied
				try {
					Double.valueOf(contents);
					contents = " 0+" + contents;
				} catch (Exception e) {

				}
				buildOrderOfOperationsNode(contents, childNode, nodeMap);
			}
			aString = aString.replaceFirst("\\Q" + firstParenthetical + "\\E", nodeKey);
			firstParenthetical = getFirstTopLevelParenthetical(aString);
			i_par++;
		}

		this.buildOrderOfOperationsNode(aString, expNode, nodeMap);

		return expNode;
	}
	/**
	 * Builds an ExpressionNode that is composed entirely of algebraic relations
	 * (ie no function calls or parentheticals)
	 * @param expression
	 * @param parentNode
	 * @param nodeMap
	 */
	public void buildOrderOfOperationsNode(String expression, ExpressionNode parentNode,
			HashMap<String, ExpressionNode> nodeMap) {
		expression = expression.replaceAll(" ", "");

		// System.out.println(expression);
		Set<String> operationSet = ExpressionParser.doubleOperationMap.keySet();
		boolean hasOp = false;
		for (String op : operationSet) {
			if (expression.contains(op)) {
				hasOp = true;
				break;
			}
		}
		if (!hasOp) {
			ExpressionNode numNode = createNodeOrReturnExistingNode(expression, nodeMap);
			parentNode.addChild(numNode);
			return;
		}

		int i_expnode = 0;
		for (String operation : operationSet) {
			Pair<Integer, String> indExpressionPair = consolidateOperation(expression, operation, i_expnode, nodeMap);
			i_expnode = indExpressionPair.getFirst();
			expression = indExpressionPair.getSecond();
		}
		// The final expression should key into the final consolidated
		// ExpressionNode
		ExpressionNode consolidatedOperationNode = createNodeOrReturnExistingNode(expression, nodeMap);
		if (consolidatedOperationNode == null) {
			System.out.println();
		}
		parentNode.addChild(consolidatedOperationNode);
	}
	
	/**
	 * Takes the current version of the expression and resolves all instances of
	 * a particular operation type (+,-,*,/,^).
	 * @param expression
	 * @param operation
	 * @param i_expnode
	 * @param nodeMap
	 * @return Pair with the last used node index and the current version of the expression.
	 */
	private Pair<Integer, String> consolidateOperation(String expression, String operation, int i_expnode,
			HashMap<String, ExpressionNode> nodeMap) {

		String valRegExp = "(\\-?[A-Za-z_]+\\d?+|" + ExpressionParser.negNumericRegExp + ")";
		String escapedOperation = "\\" + operation;
		// Reconcile exponents into their own node
		Matcher expMatcher = Pattern.compile(valRegExp + escapedOperation + valRegExp).matcher(expression);
		if (operation.equals("+") || operation.equals("-")) {
			expMatcher = Pattern.compile(valRegExp + "\\+" + valRegExp + "|" + valRegExp + "\\-" + valRegExp)
					.matcher(expression);
		}
		while (expMatcher.find()) {
			String match = expMatcher.group();
			// If + or - then try to split by + first then - second
			if (match.contains("+") && operation != "+") {
				System.out.println(match);
			}
			String[] splitMatch = match.split(escapedOperation);
			if (operation.equals("+") || operation.equals("-")) {
				splitMatch = match.split("\\+");
				operation = "+";
				if (splitMatch.length == 1) {
					splitMatch = match.split("\\-");
					operation = "-";
				}
				if (splitMatch.length == 3) {
					splitMatch = new String[] { "-" + splitMatch[1], splitMatch[2] };
				}
			}

			ExpressionNode expNode = new ExpressionNode();
			ExpressionNode firstTerm = createNodeOrReturnExistingNode(splitMatch[0], nodeMap);
			ExpressionNode secondTerm = createNodeOrReturnExistingNode(splitMatch[1], nodeMap);
			expNode.addChild(firstTerm);
			expNode.addChild(secondTerm);
			expNode.addSiblingFunction(ExpressionParser.doubleOperationMap.get(operation));
			String expNodeName = ExpressionParser.EXP_NODE + String.valueOf(i_expnode);
			nodeMap.put(expNodeName, expNode);
			expression = expression.replaceFirst("\\Q" + match + "\\E", expNodeName);
			expMatcher = Pattern.compile(valRegExp + escapedOperation + valRegExp).matcher(expression);
			if (operation.equals("+") || operation.equals("-")) {
				expMatcher = Pattern.compile(valRegExp + "\\+" + valRegExp + "|" + valRegExp + "\\-" + valRegExp)
						.matcher(expression);
			}
			i_expnode++;
		}
		return new Pair<Integer, String>(new Integer(i_expnode), expression);
	}
	
	/**
	 * Method that interprets the provided string as either a numeric value, a symbolic variable, or a
	 * node that already exists in the node map and returns an ExpressionNode accordingly.
	 * @param aString
	 * @param nodeMap
	 * @return
	 */
	private ExpressionNode createNodeOrReturnExistingNode(String aString, HashMap<String, ExpressionNode> nodeMap) {
		// First try to create a numeric node
		try {
			return createNumericNode(aString);
		} catch (Exception e) {
			// If the string cannot be converted to a double then check in the
			// map
			// If the node has a negative sign account for this with a negate
			// function
			// Determine if the string is a node key
			String nodeRegExp = "(\\-?" + ExpressionParser.EXP_NODE + "\\d+|\\-?" + ExpressionParser.NODE + "\\d+)";
			Matcher nodeMatcher = Pattern.compile(nodeRegExp).matcher(aString);
			if (nodeMatcher.find()) {
				String nodeKey = nodeMatcher.group();
				if (nodeKey.startsWith("-")) {
					return nodeMap.get(nodeKey.substring(1))
							.addFunctionalExppression(ExpressionParser.mathFunctionMap.get("NEGATE"));
				} else {
					return nodeMap.get(nodeKey);
				}
			}
			// If not a node key then the string is a variable
			ExpressionNode variableNode = new ExpressionNode();
			if (aString.startsWith("-")) {
				variableNode.seedValue(aString.substring(1));
				return variableNode.addFunctionalExppression(ExpressionParser.mathFunctionMap.get("NEGATE"));
			} else {
				variableNode.seedValue(aString);
				return variableNode;
			}
		}
	}

	public ExpressionNode createNumericNode(String val) {
		ExpressionNode numNode = new ExpressionNode();
		numNode.seedValue(Double.valueOf(val));
		return numNode;
	}

	private String prepareExpressionString(String expression) {
		Matcher m = Pattern.compile("\\^\\+?" + negNumericRegExp).matcher(expression);
		while (m.find()) {
			String expStr = m.group();
			Matcher numMatcher = Pattern.compile("\\+?" + negNumericRegExp).matcher(expStr);
			numMatcher.find();
			String numValue = numMatcher.group();
			String replacementStr = "^(" + numValue + ")";

			expression = expression.replaceAll("\\" + expStr, replacementStr);
		}

		return expression;
	}


	
}
