package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import expressionparser.ExpressionNode;
import expressionparser.ExpressionParser;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;


public class MattsLabController implements Initializable{
	public static final String numericRegEx = "\\d+(,\\d+)*(\\.\\d+(e\\d+)?)?";
	public static final String negNumericRegExp = "\\-?" + numericRegEx;
	private static ObservableMap<String,Double> variableMap = FXCollections.observableHashMap();
	private static ObservableMap<String,ExpressionNode> expressionMap = FXCollections.observableHashMap();
	
	//FX Elements
	public TextArea expressionInput;
	@FXML public TableView<ExpressionEntry> expressionTable;
	@FXML public TableColumn<ExpressionEntry,String> ExpName;
	@FXML public TableColumn<ExpressionEntry,String> Expression;
	@FXML public TableView<VariableEntry> variablesTable;
	@FXML public TableColumn<VariableEntry,String> Name;
	@FXML public TableColumn<VariableEntry,Double> Value;
	
	private ObservableList<VariableEntry> variableEntries = FXCollections.observableArrayList();
	private ObservableList<ExpressionEntry> expressionEntries = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.initializeListeners();
		expressionInterpretationList.add(aStr -> tryGetVariable(aStr));
		expressionInterpretationList.add(aStr -> tryGetAlgebraicExpression(aStr));
		expressionInterpretationList.add(aStr -> tryExpressionCall(aStr));
		//Variables table controls
		this.variablesTable.setVisible(true);
		this.variablesTable.setEditable(false);
		Name.setCellValueFactory(new PropertyValueFactory<VariableEntry,String>("name"));
		Value.setCellValueFactory(new PropertyValueFactory<VariableEntry,Double>("value"));
		this.variablesTable.setItems(this.variableEntries);
		this.variablesTable.setPlaceholder(new Label("Variables"));
		//Expression table controls
		this.expressionTable.setVisible(true);
		this.expressionTable.setEditable(false);
		ExpName.setCellValueFactory(new PropertyValueFactory<ExpressionEntry,String>("name"));
		Expression.setCellValueFactory(new PropertyValueFactory<ExpressionEntry,String>("expression"));
		this.expressionTable.setItems(this.expressionEntries);
		this.expressionTable.setPlaceholder(new Label("Expressions"));
	}
	
	
	private List<Function<String,Boolean>> expressionInterpretationList = new ArrayList<Function<String,Boolean>>();
	
	
	public void onExpressionInputAreaChanges(){
		expressionInput.setOnKeyPressed(event -> {
			if(event.getCode() == KeyCode.ENTER){
				interpretInputArea(expressionInput.getText());
			}
		});
	}
	private void initializeListeners(){
		MattsLabController.variableMap.addListener((MapChangeListener.Change<? extends String,? extends Double> change) ->{
			if (change.wasAdded()){
				variableEntries.clear();
				for (Entry<String,Double> mapEntry : variableMap.entrySet()){
					variableEntries.add(new VariableEntry(mapEntry.getKey(),mapEntry.getValue()));
				}
				this.variablesTable.setItems(variableEntries);
				this.variablesTable.refresh();
			}
		    
	    });
		
		MattsLabController.expressionMap.addListener((MapChangeListener.Change<? extends String,? extends ExpressionNode> change) ->{
			if (change.wasAdded()){
				expressionEntries.clear();
				for (Entry<String,ExpressionNode> mapEntry : expressionMap.entrySet()){
					expressionEntries.add(new ExpressionEntry(mapEntry.getKey(),mapEntry.getValue().getExpression()));
				}
				this.expressionTable.setItems(expressionEntries);
				this.expressionTable.refresh();
			}
		    
	    });
	}
	public void interpretInputArea(String inputAreaText){
		String[] splitString = inputAreaText.split("\\n");
		final String newInput = splitString[splitString.length - 1];
		
		expressionInterpretationList.stream()
			.map(func -> func.apply(newInput))
			.filter(val ->val == true).findFirst();
	}
	
	private boolean tryGetVariable(String maybeVariable){
		if (maybeVariable.contains("=")){
			String[] splitMaybeVar = maybeVariable.split("=");
			splitMaybeVar = Stream.of(splitMaybeVar)
					.map(val -> val.trim())
					.collect(Collectors.toList())
					.toArray(new String[]{});
			if (splitMaybeVar.length != 2){
				return false;
			} else {
				try{
					variableMap.put(splitMaybeVar[0], Double.parseDouble(splitMaybeVar[1]));
					return true;
				} catch (Exception e) {
					if (variableMap.containsKey(splitMaybeVar[1])){
						variableMap.put(splitMaybeVar[0], variableMap.get(splitMaybeVar[1]));
						return true;
					}
					return false;
				}
			}
		} else {
			return false;
		}
	}

	private boolean tryGetAlgebraicExpression(String maybeExp){
		List<String> splitMaybeExp = Stream.of(maybeExp.split("="))
				.map(str ->str.trim())
				.collect(Collectors.toList());
		if (splitMaybeExp.size() == 2){
			Matcher matcher = Pattern.compile("@\\((.+?)\\)").matcher(splitMaybeExp.get(1));
			if (matcher.find()){
				String expVars = matcher.group();
				String expressionStr = splitMaybeExp.get(1).replace(expVars, "").trim();
				//Remove @ sign and parenthesis
				expVars = expVars.trim().replace("@", "").replace("(", "").replace(")", "");
				List<String> splitExpVars = Stream.of(expVars.split(","))
						.map(str ->str.trim())
						.collect(Collectors.toList());
				//Attempt to parse the expression
				ExpressionNode expNode = new ExpressionNode();
				try {
					(new ExpressionParser()).compileExpression(expressionStr, expNode);
				} catch (Exception e){
					System.out.println("Could not parse expression: " + expressionStr);
					return false;
				}
				//If parse was successful, add a map of variable names to the expression node
				LinkedHashMap<String,Double> varMap = new LinkedHashMap<String,Double>();
				for (String varName : splitExpVars){varMap.put(varName, 0.0);}
				expNode.setExpression(expressionStr);
				expNode.setVariableMap(varMap);
				expressionMap.put(splitMaybeExp.get(0), expNode);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}
	
	private boolean tryExpressionCall(String maybeFuncCall){
		List<String> splitMaybeExp = Stream.of(maybeFuncCall.split("="))
				.map(str ->str.trim())
				.collect(Collectors.toList());
		if (splitMaybeExp.size() == 1){
			Double value = trySolveFuncHandle(maybeFuncCall);
			if(value != null){expressionInput.appendText("\n" + String.valueOf(value));}
			return true;
		} else if (splitMaybeExp.size() == 2){
			Double value = trySolveFuncHandle(splitMaybeExp.get(1));
			variableMap.put(splitMaybeExp.get(0), value);
			return true;
		}
		return true;
	}
	
	private Double trySolveFuncHandle(String maybeFuncHandle){
		//determine if just a function call
		Matcher matcher = Pattern.compile(".+\\(").matcher(maybeFuncHandle);
		if(matcher.find()){
			String funcHandle = matcher.group().trim().replace("(", "");
			ExpressionNode expNode = expressionMap.get(funcHandle);
			//Return null for unrecognized function handle
			if (expNode == null){
				expressionInput.appendText("\n-> Function with handle " + funcHandle + " not defined.");
				return null;
			}
			maybeFuncHandle = maybeFuncHandle.replace(funcHandle, "").replace("(", "").replace(")", "");
			List<Double> varVals = Stream.of(maybeFuncHandle.split(","))
										 .map(var -> {
											 try{
												 return Double.valueOf(var);
											 } catch (Exception e){
												 return variableMap.get(var);
											 }
										 }).collect(Collectors.toList());
			if (varVals.contains(null)){
				expressionInput.appendText("\n-> Could not evaluate function with handle " + funcHandle);
				return null;
			}
			int numVars = varVals.size();
			int numVarsReq = expNode.getVariableMap().size();
			if (numVars != numVarsReq){
				expressionInput.appendText("\n-> " + funcHandle + " expects " 
										+ String.valueOf(numVarsReq)
										+ " variables, not " + String.valueOf(numVars)
										);
				return null;
			}
			Iterator<String> keyIter = expNode.getVariableMap().keySet().iterator();
			
			for (int i_var=0;i_var<numVars;i_var++){
				String key = keyIter.next();
				expNode.getVariableMap().replace(key, varVals.get(i_var));
			}
			return expNode.solve();
		}
		return null;
	}
	
	public static class VariableEntry{
		private final SimpleStringProperty name;
		private final SimpleDoubleProperty value;
		public VariableEntry(String varName, Double varValue){
			name = new SimpleStringProperty(varName);
			value = new SimpleDoubleProperty(varValue);
		}
		public String getName() {
			return name.getValue();
		}
		public Double getValue() {
			return value.getValue();
		}

	}
	
	public static class ExpressionEntry{
		private final SimpleStringProperty name;
		private final SimpleStringProperty expression;
		public ExpressionEntry(String varName, String exp){
			name = new SimpleStringProperty(varName);
			expression = new SimpleStringProperty(exp);
		}
		public String getName() {
			return name.getValue();
		}
		public String getValue() {
			return expression.getValue();
		}

	}




}
