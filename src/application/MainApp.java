package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application{

	@Override
	public void start(Stage primaryStage) throws IOException {
		
		Parent root = FXMLLoader.load(getClass().getResource("MattsLab.fxml"));
		
		primaryStage.setTitle("Matts Lab");
		primaryStage.setScene(new Scene (root, 600,450));
		
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
